import re
import tkinter.font
import tkinter as tk
from random  import shuffle
from tkinter import messagebox

import edit

class Exam(tk.Toplevel):
    def __init__(self, s, root):
        global settings
        global db
        settings=s
        db=settings.db

        super().__init__()
        self.resizable(False, False)

        self.root=root
        if settings.review:
            self.chosen=settings.review_list
        else:
            self.chosen=settings.pick()
        self.create_widgets()

        # each value of the list refers to the regular and reverse cases
        self.is_first_time=[True, True]
        self.first_time_failed=[set(), set()]
        self.exam=self.chosen.copy()
        self.start_exam()
        
    def start_exam(self):
        shuffle(self.exam)
        self.i=self.exam.pop()
        self.failed=set()
        self.round=1
        self.previous_word=-1
        self.question['text']=db.questions[self.i]
        self.wd[self.i].mark_as_actual()
            
    def next_round(self):
        f=len(self.failed)
        text=text2='Round {} - Failed {}'.format(self.round, f)
        if f==0:
            text2='Finished in {} round'.format(self.round)
            if self.round > 1: text2 += 's'
            text2 += '!'
        self.question['text']=''
        messagebox.showinfo('', text2)
        self.old_round['text']=text
        self.old_answer['text']=''
        self.old_answer.unbind('<1>')
        self.wrong['text']=''
        self.round+=1
        actual_reverse=int(settings.reverse)
        if self.is_first_time[actual_reverse]:
            self.first_time_failed[actual_reverse]=self.failed
            self.is_first_time[actual_reverse]=False
            # Update the times counter only after the first round is complete.
            for i in self.chosen:
                db.times[i] += 1
            settings.current_review_list=self.chosen
        if self.failed:
            for i in self.failed:
                db.fails[i] += 1
            self.exam=list(self.failed)
            shuffle(self.exam)
            self.i=self.exam.pop()
            # this is to force that the first word of a round
            # is different to the last word of the previous one:
            if self.previous_word == self.i and len(self.failed)>1:
                other=self.exam.pop()
                self.exam.append(self.i)
                shuffle(self.exam)
                self.i=other
            self.question['text']=db.questions[self.i]
            for w in self.failed:
                self.wd[w].toggle_status('neutral')
            self.wd[self.i].mark_as_actual()
            self.failed=set()
        else:
            again=Again(self)
            again.transient(self)
            again.grab_set()
            self.wait_window(again)

    def create_widgets(self):

        self.title("Tangobulario")
       
        # frame with words
        self.words_frame=tk.Frame(self, bg='gray80')
        self.words_frame.pack(padx=10, pady=20, expand=0, anchor="n")
        self.create_words_labels()

        # question #
        self.question=tk.Label(self, text="", pady=10, font=("Helvetica", 24))
        self.question.pack(fill=tk.BOTH)

        # text #
        self.text_input=tk.Text(self,
            height=1, width=40, font=("Helvetica", 24))
        self.text_input.pack(fill=tk.BOTH, pady=25, padx=10)
        self.text_input.focus_set()
        self.text_input.bind("<Return>", self.get_answer)

        # wrong answer
        self.wrong=tk.Label(self, text="", fg='gray45', font=(None, 11), anchor="w")
        self.wrong.pack(fill=tk.X, padx=11)

        # status bar #
        self.status_bar=tk.Frame(self)
        self.status_bar.pack(fill=tk.BOTH, side=tk.BOTTOM, pady=2)
        self.old_answer=tk.Label(
                self.status_bar, text="", padx=10, font=("Helvetica", 16))
        self.old_answer.pack(side=tk.LEFT)
        self.old_round=tk.Label(
                self.status_bar, text="", anchor="e", padx=10, font=("Helvetica", 16))
        self.old_round.pack(side=tk.RIGHT)

    def get_answer(self, event=None):
        answer=self.text_input.get(1.0, tk.END).strip()
        self.text_input.delete(1.0, tk.END)
        question=db.questions[self.i]
        right_answer=db.answers[self.i]
        solution= question + " = " + right_answer
        self.old_answer.bind('<1>', self.wd[self.i].create_word_dialog)
        if is_correct(answer, right_answer) or answer=='=':
            self.old_answer['text']=solution
            self.old_answer['fg']="green"
            self.wd[self.i].toggle_status('guessed')
            self.wrong['text']=""
        else:
            self.failed.add(self.i)
            self.old_answer['text']=solution
            self.old_answer['fg']="red"
            self.wd[self.i].toggle_status('failed')
            self.wrong['text']=answer
            self.previous_word=self.i
        if self.exam:
            self.i=self.exam.pop()
            self.question['text']=db.questions[self.i]
            self.wd[self.i].mark_as_actual()
        else:
            self.next_round()

    def create_words_labels(self):
        family='Helvetica'
        size=12
        self.font=tkinter.font.Font(root=self.master, family=family, size=size)
        self.wd={}
        n=len(self.chosen)
        ordered_chosen=sorted(self.chosen, key=lambda x: db.questions[x]) 
        w=self.get_maximum_width()
        ncolumns=estimate_ncolumns(n,w)
        for k, i in enumerate(ordered_chosen):
            l=WordLabel(self, i, self.font)
            self.wd[i]=l
            l.config(width=w)
            x, y = divmod(k, ncolumns)
            l.grid(row=x, column=y, padx=10, pady=7, ipadx=0, ipady=0)

    def get_maximum_width(self):
        w=15 # min size of label
        for i in self.chosen:
            w1=self.font.measure(db.questions[i])//8
            w2=self.font.measure(db.answers[i])//8
            # the method "measure" gives dimension in pixels,
            # to get the text units I divide by 8
            # but I don't know if there is a better way
            w=max(w, w1, w2)
        w = min (w, 25) # max size of label
        return w 


class WordLabel(tk.Label):

    def __init__(self, master, i, font):
        super().__init__(master.words_frame)
        self.master=master
        self.i=i
        self.question=db.questions[i]
        self.answer=db.answers[i]
        self.toggle_status('neutral')
        self['text']=self.question
        self['font']=font
        self.bind("<Enter>", self.flip2)
        self.bind("<Leave>", self.flip1)
        self.bind("<Button-1>", self.create_word_dialog) 

    def flip1(self, event=None):
        self.config(text=self.question)

    def flip2(self, event=None):
        self.config(text=self.answer)

    def toggle_status(self, status):
        colors={'neutral': {'fg': 'black',  'bg': 'gray85' },
                'guessed': {'fg': 'gray50', 'bg': 'gray80' },
                'failed':  {'fg': 'red',    'bg': 'gray80' }}
        self.status=status
        for key, value in colors[status].items():
            self[key]=value
                
    def create_word_dialog(self, event=None):
        dialog=WordDialog(self) 
        dialog.transient(self.master)
        dialog.wait_visibility()
        dialog.grab_set()
        self.wait_window(dialog)

    def mark_as_actual(self):
        self['fg']='blue'
        

class WordDialog(tk.Toplevel):
    def __init__(self, word):
        super().__init__()
        self.resizable(True, False)
        self.word=word
        self.title('Word')
        # question and answer entries
        wframe=tk.Frame(self)
        self.question=tk.StringVar(value=self.word.question)
        ql=tk.Label(wframe, text=self.word.question)
        qe=tk.Entry(wframe, textvariable=self.question)
        self.answer=tk.StringVar(value=self.word.answer)
        al=tk.Label(wframe, text=self.word.answer)
        ae=tk.Entry(wframe, textvariable=self.answer)
        # radio buttons
        rframe=tk.Frame(self)
        self.v=tk.StringVar()
        self.v.set(self.word.status)
        r1=tk.Radiobutton(rframe,
                text="Neutral", variable=self.v, value='neutral')
        r2=tk.Radiobutton(rframe,
                text="Guessed", fg='Green', variable=self.v, value='guessed')
        r3=tk.Radiobutton(rframe,
                text="Failed", fg='red', variable=self.v, value='failed')
        # buttons
        bframe=tk.Frame(self)
        ok=tk.Button(bframe, text='OK',command=self.apply_changes)
        self.bind('<Return>', self.apply_changes)
        cancel=tk.Button(bframe, text='Cancel', command=self.destroy)
        self.bind('<Escape>', lambda e: self.destroy())

        wframe.pack(pady=15, padx=10)
        ql.grid(row=0, column=0, padx=5)
        qe.grid(row=0, column=1, ipady=2)
        al.grid(row=1, column=0, padx=5)
        ae.grid(row=1, column=1, ipady=2)
        rframe.pack(pady=5, padx=8)
        r1.pack(side=tk.LEFT)
        r2.pack(side=tk.LEFT)
        r3.pack(side=tk.LEFT)
        bframe.pack(fill=tk.BOTH, padx=30, pady=10)
        ok.pack(side=tk.LEFT)
        cancel.pack(side=tk.RIGHT)


    def apply_changes(self, event=None):
        self.destroy()
        self.apply_edit_changes()
        self.apply_status_change()

    def apply_edit_changes(self):
        question=self.question.get().strip()
        if edit.validate(question):
            self.word.question=question
            self.word.config(text=question)
            db.questions[self.word.i]=question
            if self.word.i == self.word.master.i:
                self.word.master.question.config(text=question)
        
        answer=self.answer.get().strip()
        if edit.validate(answer):
            self.word.answer=answer
            db.answers[self.word.i]=answer
        
    def apply_status_change(self):
        old_status=self.word.status
        new_status=self.v.get()
        if old_status==new_status:
            return
        self.word.toggle_status(new_status)
        i=self.word.i
        master=self.word.master
        master.wrong['text']=''
        master.old_answer['text']=''
        master.old_answer.unbind('<1>')
        if old_status=='neutral':
            try:
                master.exam.remove(i)
                if new_status=='failed':
                    master.failed.add(i)
            except ValueError:
                # the word is the current word being asked
                master.text_input.delete(1.0, tk.END)
                if new_status=='guessed':
                    master.text_input.insert(1.0, self.word.answer)
                master.get_answer()
        if old_status=='guessed':
            if new_status=='neutral':
                master.exam.append(i)
                shuffle(master.exam)
            if new_status=='failed':
                master.failed.add(i)
        if old_status=='failed':
            master.failed.remove(i)
            if new_status=='neutral':
                master.exam.append(i)
                shuffle(master.exam)
                

class Again(tk.Toplevel):
    def __init__(self, master):
        super().__init__(master)
        self.master=master
        l=tk.Label(self, text="Again?", font=(None, 14))
        l.pack(pady=20)

        bframe=tk.Frame(self)
        bframe.pack(padx=20, pady=10, fill=tk.BOTH)
        
        t='Repeat the quiz with the same words'
        yesl=tk.Label(bframe, anchor="w", padx=4, text=t)
        yes=tk.Button(bframe, width=4, text='Yes', underline=0,
                command=self.yes_function)
        self.bind('y', lambda e: yes.invoke())
        yes.grid(column=0, row=0, pady=4, sticky="w")
        yesl.grid(column=1, row=0, sticky="w")

        t='Repeat only the failed questions'
        self.failedl=tk.Label(bframe, anchor="w", padx=4, text=t)
        self.failed=tk.Button(bframe, width=4, text='Failed', underline=0,
                command=self.failed_function)
        self.bind('f', lambda e: self.failed.invoke())
        self.failed.grid(column=0, row=1, pady=4, sticky="w")
        self.failedl.grid(column=1, row=1, sticky="w")
        yes.focus_set()

        t='Start a quiz with a new set of words'
        newl=tk.Label(bframe, anchor="w", padx=4, text=t)
        new=tk.Button(bframe, width=4, text='New', underline=0,
                command=self.new_function)
        self.bind('n', lambda e: new.invoke())
        new.grid(column=0, row=2, pady=4, sticky="w")
        newl.grid(column=1, row=2, sticky="w")

        dframe=tk.Frame(self)
        dframe.pack(padx=10, pady=10, fill=tk.BOTH)
        t=' Reverse'
        self.rev=tk.Checkbutton(dframe, text=t, variable=master.master.reverse,
                underline=1, command=self.reverse_function)
        self.bind('r', self.rev_shortcut)
        self.rev.pack(side='left', padx=8)
        
        t=' Save statistics'
        self.save_v=tk.BooleanVar()
        self.save_v.set(True)
        self.save=tk.Checkbutton(dframe,
                text=t, variable=self.save_v, underline=1)
        self.bind('s', self.save_shortcut)
        self.save.pack(side='left', padx=8)

        t='Exit application'
        quit=tk.Button(dframe, width=4, text='Quit', underline=0,
                command=self.quit_function)
        self.bind('q', lambda e: quit.invoke())
        quit.pack(side='right', padx=8)

        self.change_reverse_state(int(settings.reverse))

        self.bind('<Return>', self.click_button)
        self.protocol('WM_DELETE_WINDOW', self.quit_function)

    def change_reverse_state(self, actual_reverse):
        if self.master.first_time_failed[actual_reverse]:
            self.failed['state']='normal'
            self.failedl['state']='normal'
        else:
            self.failed['state']='disabled'
            self.failedl['state']='disabled'

    def click_button(self, event=None):
        widget=self.focus_get()
        if widget != self:
            widget.invoke()

    def yes_function(self, event=None):
        self.save_function()
        for w in self.master.chosen:
            self.master.wd[w].toggle_status('neutral')
        self.master.exam=self.master.chosen.copy()
        self.master.start_exam()
        self.destroy()

    def failed_function(self, event=None):
        self.save_function()
        actual_reverse=int(settings.reverse)
        for w in self.master.first_time_failed[actual_reverse]:
            self.master.wd[w].toggle_status('neutral')
        self.master.exam=list(self.master.first_time_failed[actual_reverse])
        self.master.start_exam()
        self.destroy()

    def reverse_function(self):
        self.master.master.update_reverse()
        for l in self.master.wd.values():
            l.question=db.questions[l.i]
            l.answer=db.answers[l.i]
            l.config(text=l.question)
        self.change_reverse_state(int(settings.reverse))

    def rev_shortcut(self, event=None):
        self.rev.invoke()
        self.focus_set()

    def new_function(self, event=None):
        self.save_function()
        self.master.destroy()

    def quit_function(self, event=None):
        self.save_function()
        self.master.root.quit_signal=True
        self.master.destroy()
    
    def save_function(self):
        if self.save_v.get():
            try:
                db.save(settings.filename)
            except Exception:
                tk.messagebox.showerror(
                        '', "Error saving file "+settings.filename)

    def save_shortcut(self, event=None):
        self.save.invoke()
        self.focus_set()


def estimate_ncolumns(n,w):
    ncolumns = 4
    if w>20: ncolumns = 3
    if n>32: ncolumns += 1
    return ncolumns

def is_correct(answer, right_answer):
    """If the word contains a parenthesized expression at the end,
    the answer is considered correct although it is omitted"""
    a=right_answer
    if answer == a:
        return True
    # a is never the empty string
    if a[-1]==')'  and  '(' in a:
        i=a.rfind('(')
        a=a[0:i]
    a=a.strip()
    if a == '': return False
    return answer == a




