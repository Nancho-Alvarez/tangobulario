import tkinter as tk
from tkinter.scrolledtext import ScrolledText

import init


class HelpWindow(tk.Toplevel):
    def __init__(self, master):
        super().__init__()

        self.protocol("WM_DELETE_WINDOW", self.close)
        self.bind('<Escape>', self.close)
        self.geometry("650x850")
        
        h=ScrolledText(self, wrap=tk.WORD, font=(None, 16),
                borderwidth=10, bg='white', relief='flat')
        h.pack(padx=10, pady=10, fill=tk.BOTH, expand=True)
        h.bind('<1>', lambda e: h.focus_set())
        
        #button
        button=tk.Button(self, text="Close", command=self.close)
        button.pack(pady=15)

        # styles
        h.tag_config('txt', foreground='black')
        h.tag_config('title', font=(None, 28))
        h.tag_config('section', font=(None, 22, 'bold'))
        h.tag_config('subsection', font=(None, 18, 'bold'), foreground='#633')
        h.tag_config('item', font=(None, 18), foreground='#363')
        h.tag_config('code', font=('Monospace', 18), foreground='#662')
        h.tag_config('colored_word', foreground='#2020d0')
        h.tag_config('option', foreground='#00f', font=('Monospace', 20))
        h.tag_config('signature', font=(None, 12))


        # INTRODUCTION
        h.insert(tk.INSERT, '\nTangobulario\n\n', 'title')

        text="""
        Tangobulario is a small app to practice vocabulary.
        The name is a play on words: "tango" in Japanese means vocabulary.

        The program uses a database that contains pairs of words in two
        languages.
        When you start a quiz, a list of words is chosen at random.
        The program shows them on screen and
        asks for their translation in random order.
        Words that are answered correctly disappear from the list
        and are not asked again in the following rounds.

        The program keeps a statistic of the number of hits of each word
        that is used to chose the words in future quizzes.
        """
        h.insert(tk.INSERT, fmt(text), 'txt')


        # DATABASES
        h.insert(tk.INSERT, '\n\nDatabases\n', 'section')

        text="""
        Word databases are normal text files. Each line contains a word and
        its translation, separated by the "=" sign. It is also
        possible to include labels with the name of a section in brackets.
        Any other line is considered a comment and is ignored.

        The user must create a database with all the vocabulary that he wishes
        to practice. The ideal is to have a custom file in which you add new
        words as you learn them. To begin, copy the following example to a file
        called words.txt:

        """
        h.insert(tk.INSERT, fmt(text), 'txt')

        text="""
        [animals]
        lion=león
        tiger=tigre
        dog=perro

        [colors]
        red=rojo
        yellow=amarillo
        """
        h.insert(tk.INSERT, fmt_code(text), 'code')



        # STATISTICS
        text="\nStatistics\n"
        h.insert(tk.INSERT, text, 'section')
        
        text="""
        After each quiz, the database is modified by adding
        to each line two numerical values:
        """
        h.insert(tk.INSERT, fmt(text), 'txt')
        
        h.insert(tk.INSERT, ' times ', 'colored_word')

        text="""
        (the number of times a word
        has been chosen in previous quizzes) and
        """
        h.insert(tk.INSERT, fmt(text), 'txt')
        
        h.insert(tk.INSERT, ' fails ', 'colored_word')

        text="""
        (the number of incorrect answers to that word).
        Note that times only is increased once in each
        quiz, but fails can be increased several times.

        Based on these values, each word is assigned a weight. For example, a
        word with weight 3 is 6 times more likely to be chosen than one with
        weight 0.5. Words that have never been chosen are treated as the word
        with the highest weight. The formula is:

        """
        h.insert(tk.INSERT, fmt(text), 'txt')

        image=tk.PhotoImage(file="weight.png")
        h.image_create(tk.END, image=image)
        h.image=image # http://effbot.org/pyfaq/why-do-my-tkinter-images-not-appear.htm

        h.insert(tk.INSERT, '\nwhere', 'txt')
        h.insert(tk.INSERT, ' exponent ', 'colored_word')
        
        text="""
       
        is a real number that can be modified
        inside the app. The value 0 cancels the effect of
        the statistics. The default value is {}.

        """.format(init.default_e)
        h.insert(tk.INSERT, fmt(text), 'txt')
        h.insert(tk.INSERT, '\n')


        # MAIN WINDOW
        h.insert(tk.INSERT, 'Main window\n', 'section')
        h.insert(tk.INSERT, '\n')
        text="""
        This window shows a list of all the words in the database.
        You can choose a different database by clicking the button
        'Open file'.

        If you hover the mouse over each word, its translation is shown.
        You can restrict the number of words in the list by selecting
        only some sections from the box at the left.
        The total number of active words is displayed just below
        the list.  If the database contains many words they are arranged
        in several pages.

        The list of active words can be ordered alphabetically, by their
        relative probability (weight) or by the default order in the
        database. The order can be ascending or descending.
      
        Each word has a background colour that represents its
        probability of being chosen in quizzes: red means that
        the word has been failed several times in previous quizzes so it
        is a 'difficult' word and is
        more likely to be chosen in the future, the closer to pure red,
        the higher probability; green means the opposite, the word has been
        answered correctly in the past so it is an 'easy' word,
        the closer to pure green the lower
        probability of being chosen in the future.

        White background means that the word is new in the database
        and has not yet being included in any quiz. In that case, the
        word is assigned the same probability as if it were pure red.

        The relative probability of the words can be altered with the
        'exponent' slide bar. A big value means that the probability
        assigned to red words is much greater than the assigned to
        green ones; a value of zero cancels the effect of probalities: 
        all words are equiprobable; a negative value reverses the effect
        of the probabilities: the easy words are more likely to be chosen
        than the difficult ones.

        To start a quiz press the button 'Start'. You can change the number 
        of questions of each quiz, it has to be a number between 1 and 80.
        Also you swap the questions and answers.

        The list of words chosen in each quiz is random based on the
        probabilities of each word, but you can 
        use exactly the same list as in the previous run of the program 
        by ticking the option 'Last session review'.

        """
        h.insert(tk.INSERT, fmt(text), 'txt')




        # QUIZ WINDOW
        h.insert(tk.INSERT, 'Quiz window\n', 'section')
        h.insert(tk.INSERT, '\n')

        text="""
        The chosen words are displayed in a grid. Hovering the mouse over
        each word shows its translation.

        The quiz is composed by a number of rounds. 
        The words that are answered correctly
        disappear from the grid and are not asked in the
        next rounds.

        In each round the order is random, but is guaranteed that
        the first word of a round is different from the last word
        of the previous one (if there is more than one).

        The answer you type is case sensitive and has to be exactly
        the same as the answer that appears in the database.
        This enforces to know the exact spelling of the words.
        But there are two exceptions:
        \n
        1. typing a single equal sign '"""
        h.insert(tk.INSERT, fmt(text), 'txt')
        h.insert(tk.INSERT, '=', 'colored_word')
        text="""
        ' marks the answer as correct. This is useful when the answer
        contains foreign characters that you cannot
        type, or just when you know the answer and want to save some typing.

        2. If the answer ends with a parentheses, its contents can be
        omitted. These parentheses can be used in the database to
        distinguish words with several meanings:
        """
        h.insert(tk.INSERT, fmt(text), 'txt')
        text="""
        date (day, month, year)
        date (appointment)\n
        """
        h.insert(tk.INSERT, fmt_code(text), 'code')
        text="""
        Clicking one of the words in the grid opens a dialog in which you
        can edit the word (if you find some typo), or change its status
        from guessed, failed or neutral (useful for example if you
        messed up when typing).

        The last answered question, with its correct answer, is displayed
        at the left bottom. Clicking on it opens the edit dialog for
        that word.

        When all the words have been guessed correctly, a window appears 
        where you can choose between repeat the same quiz, repeat only the
        words that you failed in the first round, choose a new set of 
        words from the database, or exit the program.

        The statistic of failed words, and any other change,
        is saved in the database unless you
        untick the option 'Save statistics'.\n
        """
        h.insert(tk.INSERT, fmt(text), 'txt')



        # EDIT WINDOW
        h.insert(tk.INSERT, 'Edit window\n', 'section')
        h.insert(tk.INSERT, '\n')
        text="""
        This window is intended only to do small changes in the database file.
        For major changes, such as adding many words or rearrange sections,
        it is recommended to use a conventional text editor.\n\n
        The window displays all the pairs of words and sections in the database.
        Clicking the small icon to the left of section section name, expands
        or collapses it. Next to each word is its relative weight:
        (1+fails)/times.\n\n
        Clicking any word opens a dialog in which it is possible to edit 
        all its components: question, answer, times and fails. Also it is possible
        to delete the word or move it to other section.
        The delete and move operations are not applied immediately, instead
        the word is marked typographically and the changes are queued. All 
        pending changes will be applied with the commands 'Refresh' or 'Save'.
        Finally, in this dialog there is an option to find duplicates:
        words with the same question or answer.\n\n
        Clicking a section name opens a dialog in which it is possible
        to add an explanatory comment, change the name of the section, or 
        add more words. To speed up the addition of new words you can
        use the 'tab' and 'return' keys. To correct typos there exists the
        option to delete the last word added.\n\n"""
        h.insert(tk.INSERT, fmt(text), 'txt') 
        
        # File menu
        h.insert(tk.INSERT, 'File\n', 'subsection')
        h.insert(tk.INSERT, 'New...\n', 'item')
        text="""
        Creates a new empty database file. It is necessary to provide a file
        name for the database. If the file exists it will be overwritten.\n
        """
        h.insert(tk.INSERT, fmt(text), 'txt')
        h.insert(tk.INSERT, 'Open...\n', 'item')
        text="""
        Opens a database file. If the current database has not been saved,
        a dialog prompts for confirmation.\n
        """
        h.insert(tk.INSERT, fmt(text), 'txt')
        h.insert(tk.INSERT, 'Merge with...\n', 'item')
        text="""
        Merge the contents of the current database with other one, appending
        the new words at the end. If both databases share sections,
        their contents are merged in a section by section basis.
        Pairs of words that are identical inside the same section are
        substituted by a unique pair with the average statistics of both
        pairs. The comments of each section are also merged, and repeated
        comments are discarded.\n
        """
        h.insert(tk.INSERT, fmt(text), 'txt')
        h.insert(tk.INSERT, 'Save\n', 'item')
        text="""
        Applies any pending change and saves the database.
        Also resets the view to show all the database.\n
        """
        h.insert(tk.INSERT, fmt(text), 'txt')
        h.insert(tk.INSERT, 'Save as...\n', 'item')
        text="""
        Same as before, but asks for a file name. If the file exists it will
        be overwritten.\n
        """
        h.insert(tk.INSERT, fmt(text), 'txt')
        h.insert(tk.INSERT, 'Close\n', 'item')
        text="""
        Exits the edit window and goes back to the main window.\n
        """
        h.insert(tk.INSERT, fmt(text), 'txt')

        # Sections menu
        h.insert(tk.INSERT, 'Sections\n', 'subsection')
        h.insert(tk.INSERT, 'Add section...\n', 'item')
        text="""
        Add a new empty section.\n
        """
        h.insert(tk.INSERT, fmt(text), 'txt')
        h.insert(tk.INSERT, 'Delete section...\n', 'item')
        text="""
        Delete a section. By default the words belonging to
        that section are not deleted but moved out of any 
        section. There is an option to delete also the words.\n
        """
        h.insert(tk.INSERT, fmt(text), 'txt')
        h.insert(tk.INSERT, 'Go to section...\n', 'item')
        text="""
        Show only the chosen section.\n
        """
        h.insert(tk.INSERT, fmt(text), 'txt')
        h.insert(tk.INSERT, 'Expand all\n', 'item')
        text="""
        Show the content of all sections.\n
        """
        h.insert(tk.INSERT, fmt(text), 'txt')
        h.insert(tk.INSERT, 'Collapse all\n', 'item')
        text="""
        Hide the content of all sections.\n
        """
        h.insert(tk.INSERT, fmt(text), 'txt')

        h.insert(tk.INSERT, 'Tools\n', 'subsection')
        h.insert(tk.INSERT, 'Undo all changes\n', 'item')
        text="""
        Revert all the changes done during the edit 
        session, including opening files. Any change
        not saved will be lost.\n
        """
        h.insert(tk.INSERT, fmt(text), 'txt')
        text="""
        """
        h.insert(tk.INSERT, fmt(text), 'txt')
        h.insert(tk.INSERT, 'Remove statistics\n', 'item')
        text="""
        Delete the information regarding the number of times 
        a word has been asked, and the number of wrong answers.
        This operation can not be undone.\n
        """
        h.insert(tk.INSERT, fmt(text), 'txt')
        h.insert(tk.INSERT, 'Swap columns\n', 'item')
        text="""
        Interchange questions and answers.\n
        """
        h.insert(tk.INSERT, fmt(text), 'txt')
        h.insert(tk.INSERT, 'Find duplicates\n', 'item')
        text="""
        Search the database for pair of words that have
        the same question or the same answer.\n
        """
        h.insert(tk.INSERT, fmt(text), 'txt')
        h.insert(tk.INSERT, 'Merge identicals\n', 'item')
        text="""
        The duplicate words that are identical (same word
        and same answer) and that also are in the same section
        are merged into a single one. The statistics are averaged.\n
        """
        h.insert(tk.INSERT, fmt(text), 'txt')
        h.insert(tk.INSERT, 'Search...\n', 'item')
        text="""
        Look for words that contain a substring in the questions,
        the answers or both. The option 'extended' allows
        the substring to not be consecutive. It is also possible
        to look for words whose times and fails lie in some range.\n
        """
        h.insert(tk.INSERT, fmt(text), 'txt')

        h.insert(tk.INSERT, 'Refresh\n', 'subsection')
        text="""
        Some of the edits done in the dialogs are not applied
        immediately, namely the operations of delete a word and move
        a word to other section. This command applies any pending
        change and resets the view to show all the database.\n
        """
        h.insert(tk.INSERT, fmt(text), 'txt')
        h.insert(tk.INSERT, 'Save\n', 'subsection')
        text="""
        Same as the 'save' command in the file menu. It applies all
        pending changes, refreshes the view and saves the database.\n
        """
        h.insert(tk.INSERT, fmt(text), 'txt')

        # COMMAND LINE OPTIONS
        h.insert(tk.INSERT, 'Command line options\n', 'section')
        text="""
        If started from the command line, tangobulario supports the
        following options:\n
        """
        h.insert(tk.INSERT, fmt(text), 'txt')

        h.insert(tk.INSERT, '-#', 'option') 
        h.insert(tk.INSERT, ' (# is a natural number)\n', 'txt')
        text="""
        The number of words to use. The default value is {}.\n
        """.format(init.default_n)
        h.insert(tk.INSERT, fmt(text), 'txt')

        h.insert(tk.INSERT, '-s', 'option') 
        h.insert(tk.INSERT, ' section_1,section_2,...,section_n\n', 'txt')
        text="""
        The section or sections to be used. They must be separated by commas
        (without spaces).\n
        """
        h.insert(tk.INSERT, fmt(text), 'txt')

        h.insert(tk.INSERT, '-r\n', 'option') 
        text="""
        Reverse the direction in which the words are asked. Questions become
        answers and viceversa.\n
        """
        h.insert(tk.INSERT, fmt(text), 'txt')

        h.insert(tk.INSERT, '-v\n', 'option') 
        text="""
        Review mode. Choose exactly the same words used the previous run.\n
        """
        h.insert(tk.INSERT, fmt(text), 'txt')

        h.insert(tk.INSERT, '-e', 'option') 
        h.insert(tk.INSERT, ' exponent\n', 'txt')
        text="""
        The exponent parameter is a real number. It is used
        in the formula that assigns to each word a relative probability of
        being chosen.\n
        """
        h.insert(tk.INSERT, fmt(text), 'txt')

        text="""
        Any other argument is interpreted as a database file. 
        But only the last one is used.\n
        """

        # BUGS
        h.insert(tk.INSERT, 'Bugs\n', 'section')
        text="""
        There are probles with the focus in some of the 
        dialog windows. If some dialog box does not allow
        type text, try to commute to other window (with alt-tab)
        and back again.

        Only a single instance of this app should be running
        simultaneously.\n
        """
        h.insert(tk.INSERT, fmt(text), 'txt')



        text="\nDisclaimer\n"
        h.insert(tk.INSERT, text, 'section')

        text="""
        I am not a programmer.
        This is my first program with a graphical user interface.
        I wrote it as a way of learning python and the tkinter library. 
        I'm sure the code can be improved a lot, specially the overall design.
        But I find it useful and that's why I share it.\n
        """
        h.insert(tk.INSERT, fmt(text), 'txt')

        text="\nAbout\n"
        h.insert(tk.INSERT, text, 'section')

        text="""
        Tangobulario 2.1 (June, 2019)
        Programmed in python 3.5 with the tkinter module.
        Nancho Alvarez (vag@uma.es)
        """
        h.insert(tk.INSERT, fmt_about(text), 'signature')
        h.config(state='disabled')

    def close(self, event=None):
        self.master.help_button.config(state='normal')
        self.destroy()

def fmt(string):
    s=string[1:]
    s=s.replace('\n\n','<RETURN>')
    s=s.replace('\n', ' ')
    s=s.replace('<RETURN>','\n\n')
    s=s.replace(' '*8, '')
    return s

def fmt_code(string):
    s=string.lstrip()
    s=s.replace(' '*8, '')
    return s

def fmt_about(string):
    s=string.lstrip()
    s=s.replace(' '*8, '')
    return s

if __name__=="__main__":
    root=tk.Tk()
    h=HelpWindow(root)
    root.bind('<Escape>', lambda e:root.destroy())
    root.mainloop()
