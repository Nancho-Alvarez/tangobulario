from tkinter.filedialog import askopenfilename
from tkinter.filedialog import asksaveasfilename
import copy
from tkinter import messagebox
import os
from pathlib import Path
import re
import tempfile
import tkinter as tk
from tkinter import ttk

import init

class Edit(tk.Toplevel):
    def __init__(self, settings):
        global db
        super().__init__()
        
        self.settings=settings
        self.filename=settings.filename
        self.resizable(True, True)
        self.minsize(200,400)
        self.protocol("WM_DELETE_WINDOW", self.exit_edit)

        db=copy.deepcopy(settings.db)
        self.create_menu()
        self.create_widgets()
        self.setup()

    def setup(self):
        self.refreshed=True
        self.title(self.filename)
        self.saved=True
        self.create_lines()

    def exit_edit(self):
        if self.check_saved() == None: return
        # ugly hack: the mouse wheel is not unbind after destroying
        #self.unbind_all('<Button-4>')
        #self.unbind_all('<Button-5>')
        #self.unbind_all('<MouseWheel>')
        self.settings.filename=self.filename
        self.destroy()

    def create_menu(self):
        menubar = tk.Menu(self)

        filemenu = tk.Menu(menubar, tearoff=0)
        filemenu.add_command(label="New...", command=self.new)
        filemenu.add_command(label="Open...", command=self.open)
        filemenu.add_command(label="Merge with...", command=self.merge)
        filemenu.add_command(label="Save", command=self.save)
        filemenu.add_command(label="Save as...", command=self.saveas)
        filemenu.add_command(label="Close", command=self.exit_edit)
        menubar.add_cascade(label="File",  menu=filemenu)

        sectionsmenu = tk.Menu(menubar, tearoff=0)
        sectionsmenu.add_command(label="Add section...", command=self.add_section)
        sectionsmenu.add_command(label="Delete section...", command=self.delete_section)
        sectionsmenu.add_command(label="Go to section...", command=self.goto_section)
        sectionsmenu.add_command(label="Expand all", command=self.open_all)
        sectionsmenu.add_command(label="Collapse all", command=self.close_all)

        menubar.add_cascade(label="Sections", menu=sectionsmenu)

        toolsmenu = tk.Menu(menubar, tearoff=0)
        toolsmenu.add_command(label="Undo all changes", command=self.undo)
        toolsmenu.add_command(
                label="Remove statistics", command=self.remove_statistics)
        toolsmenu.add_command(
                label="Swap columns", command=self.swap)
        toolsmenu.add_command(label="Find duplicates", command=self.duplicates)
        toolsmenu.add_command(label="Merge identicals", command=self.identicals)
        toolsmenu.add_command(label="Search...", command=self.search)
        menubar.add_cascade(label="Tools", menu=toolsmenu)

        menubar.add_command(label="Refresh",command=self.refresh)
        menubar.add_command(label="Save",command=self.save)

        self.config(menu=menubar)

    def refresh(self):
        if self.refreshed: return
        self.refreshed = True
        for l in self.lines:
            if l.ltype == 'word':
                section  = db.all_sections[l.s_index]
                original = db.all_sections[l.s_original]
                if l.marked:
                    # delete marked words
                    db.contents[original].remove(l.i)
                elif l.s_index != l.s_original:
                    # change word section
                    db.contents[original].remove(l.i)
                    db.contents[section].append(l.i)
        self.create_lines()

    def check_saved(self):
        if self.saved == False:
            message="Save current file: {}?".format(self.filename)
            answer=messagebox.askyesnocancel(title="Save", message=message)
            if answer == None: return None
            if answer == True: self.save()
        return True

    def new(self):
        global db
        if self.check_saved() == None: return
        absolute=asksaveasfilename( title="New file", defaultextension=".txt",
                filetypes=(("txt files", "*.txt"), ("all files", "*.*")))
        if not absolute: return
        relative=relative_path(absolute)
        self.filename=relative
        db=init.Database()
        self.mark_as_not_saved()
        self.create_lines()

    def open(self):
        global db
        if self.check_saved() == None: return
        absolute=askopenfilename( title="New file", defaultextension=".txt",
                filetypes=(("txt files", "*.txt"), ("all files", "*.*")))
        if not absolute: return
        relative=relative_path(absolute)
        self.filename=relative
        db=init.Database(relative)
        self.setup()

    def merge(self):
        global db
        self.refresh()
        absolute=askopenfilename(
                title="Merge with...",
                filetypes=(("txt files", "*.txt"), ("all files", "*.*")))
        if not absolute: return
        relative=relative_path(absolute)
        tmp=tempfile.NamedTemporaryFile(mode='w', delete=False)
        tmp.close()
        db.save(tmp.name)
        db2=init.Database(relative)
        file=open(tmp.name, 'a')
        file.write(''.join(db2.comments[init.default_section]))
        file.write('[' + init.default_section + ']\n')
        lines=db2.lines_after_comment()
        file.writelines(lines)
        file.close()
        db=init.Database(tmp.name)
        self.identicals()
        os.remove(tmp.name)
        self.create_lines()

    def save(self):
        self.refresh()
        db.save(self.filename)
        self.saved=True
        self.title(self.filename)

    def saveas(self):
        absolute=asksaveasfilename(defaultextension=".txt",
                filetypes=(("txt files", "*.txt"), ("all files", "*.*")))
        if not absolute: return
        relative=relative_path(absolute)
        self.filename=relative
        self.save()

    def undo(self):
        global db
        answer=messagebox.askyesno('Confirm',
                'Revert all the changes to the beginning of the session?')
        if answer == False: return
        db=copy.deepcopy(self.settings.db)
        self.filename=self.settings.filename
        self.create_lines()
        self.mark_as_not_saved()

    def remove_statistics(self):
        answer=messagebox.askyesno('Confirm', 'Delete all the statistics?')
        if answer == False: return
        for l in self.lines:
            if l.ltype == 'word':
                i=l.i
                db.times[i]=0
                db.fails[i]=0
        for s in self.tree.get_children():
            for w in self.tree.get_children(s):
                if w.startswith('w'):
                    values=self.tree.item(w)['values']
                    values[-1]=''
                    self.tree.item(w, values=values)
        self.mark_as_not_saved()

    def swap(self):
        for l in self.lines:
            if l.ltype == 'word':
                i=l.i
                db.w1[i], db.w2[i] = db.w2[i], db.w1[i]
        for s in self.tree.get_children():
            for w in self.tree.get_children(s):
                if w.startswith('w'):
                    values=self.tree.item(w)['values']
                    values = [values[1], values[0], values[2]]
                    self.tree.item(w, values=values)
        self.mark_as_not_saved()

    def duplicates(self):
        f=set()
        d1={}
        d2={}
        for n, l in enumerate(self.lines):
            if l.ltype == 'section':
                s=n
            if l.ltype == 'word':
                q=db.questions[l.i]
                a=db.answers  [l.i]
                if q in d1.keys():
                    f.add (d1[q][0]) # section
                    f.add (d1[q][1]) # word
                    f.add (s)
                    f.add (n)
                else:
                    d1[q]=(s, n)
                if a in d2.keys():
                    f.add (d2[a][0]) # section
                    f.add (d2[a][1]) # word
                    f.add (s)
                    f.add (n)
                else:
                    d2[a]=(s, n)
        self.filter = sorted(f)
        self.display()
        self.refreshed = False
        self.open_all()

    def identicals(self):
        """If there are two words with the same question and answer
        inside the same section, we merge them into one with the average
        of the statistics (rounded up)"""
        for s in db.all_sections:
            hash={}
            purged=[]
            for i in db.contents[s]:
                q=db.questions[i]
                a=db.answers[i]
                if (q,a) in hash:
                    self.mark_as_not_saved()
                    old_i=hash[(q,a)]
                    old_times=db.times[old_i]
                    old_fails=db.fails[old_i]
                    times=db.times[i]
                    fails=db.fails[i]
                    t=average_up(times, old_times)
                    f=average_up(fails, old_fails)
                    db.times[old_i]=t
                    db.fails[old_i]=f
                else:
                    hash[(q,a)]=i
                    purged.append(i)
            db.contents[s]=purged
        self.create_lines()

    def search(self):
        search_dialog=SearchDialog(self)
        search_dialog.transient(self)
        search_dialog.wait_visibility()
        search_dialog.grab_set()
        self.wait_window(search_dialog)

    def add_section(self):
        dialog=tk.Toplevel(self)
        self.sdialog=dialog
        dialog.title('')
        label=tk.Label(dialog, text="Enter new section name")
        label.config(font=(None, 10))
        label.pack(padx=5, pady=5)
        self.new_section_name=tk.StringVar()
        entry=tk.Entry(dialog, textvariable=self.new_section_name)
        entry.config(font=(None, 14))
        entry.pack(padx=5, pady=5)
        entry.focus_set()
        entry.bind('<Return>', self.add_section_to_list)
        dialog.transient()
        dialog.grab_set()
        dialog.wait_window()

    def add_section_to_list(self, event=None):
        name=self.new_section_name.get()
        if validate(name) and name not in db.all_sections:
            db.contents[name]=[]
            db.all_sections.append(name)
            self.create_lines()
            self.mark_as_not_saved()
        self.sdialog.destroy()
        
    def delete_section(self):
        if len(db.all_sections) == 1:
            messagebox.showinfo('', 'There are no sections')
            return
        dialog=tk.Toplevel(self)
        dialog.title('')
        dialog.resizable(False, False)
        
        def next_section(event=None):
            s_index=section_strings.index( section_variable.get() )
            if event.keysym == 'Down':
                s_index=min(s_index + 1, len(section_strings)-1)
            elif event.keysym == 'Up':
                s_index=max(s_index -1, 0)
            section_variable.set( section_strings[s_index] )
        
        def delete_section_from_list(event=None):
            s=section_variable.get()
            if delete_also_words.get():
                n=len(db.contents[s])
                message='Delete ' + str(n) + ' words?'
                result=messagebox.askyesno('', message)
                if not result: return
            else:
                db.contents[init.default_section] += db.contents[s]
            db.all_sections.remove(s)
            dialog.destroy()
            self.create_lines()
            self.mark_as_not_saved()

        label=tk.Label(dialog, text="Delete section:")
        label.pack(pady=5, padx=15, anchor="w", fill=tk.X) 
        section_strings= db.all_sections[1:]
        section_variable=tk.StringVar()
        section_variable.set(section_strings[-1])
        section_menu=tk.OptionMenu(dialog,
                section_variable, *section_strings)
        section_menu.focus_set()
        section_menu.pack(pady=5, padx=15, anchor="w", fill=tk.X)
        dialog.bind('<Down>', next_section)
        dialog.bind('<Up>',   next_section)
        # option
        delete_also_words=tk.BooleanVar()
        option=tk.Checkbutton(dialog, text=' Delete also the words', 
                                                var = delete_also_words)
        option.pack(padx=10, pady=10, anchor="w", fill=tk.X)
        # buttons
        bframe=tk.Frame(dialog)
        ok=tk.Button(bframe, text='OK', command=delete_section_from_list)
        dialog.bind('<Return>', delete_section_from_list)
        cancel=tk.Button(bframe, text='Cancel', command=dialog.destroy)
        dialog.bind('<Escape>', lambda e: dialog.destroy())

        bframe.pack(fill=tk.BOTH, padx=30, pady=10)
        ok.pack(side=tk.LEFT)
        cancel.pack(side=tk.RIGHT)
        dialog.transient()
        dialog.grab_set()
        dialog.wait_window()

    def goto_section(self):
        dialog=tk.Toplevel(self)
        dialog.title('')
        dialog.resizable(False, False)

        def next_section(event=None):
            s_index=section_strings.index( section_variable.get() )
            if event.keysym == 'Down':
                s_index=min(s_index + 1, len(section_strings)-1)
            elif event.keysym == 'Up':
                s_index=max(s_index -1, 0)
            section_variable.set( section_strings[s_index] )
        
        def filter_section(event=None):
            s=section_variable.get()
            s_index=db.all_sections.index(s)
            finded=set()
            for n, l in enumerate(self.lines):
                if l.s_index==s_index:
                    finded.add(n)
            self.filter=sorted(finded) 
            self.display()
            dialog.destroy()
            self.refreshed = False

        label=tk.Label(dialog, text="Go to section:")
        label.pack(pady=5, padx=15, anchor="w", fill=tk.X) 
        section_strings= db.all_sections[1:]
        section_variable=tk.StringVar()
        section_variable.set(section_strings[-1])
        section_menu=tk.OptionMenu(dialog,
                section_variable, *section_strings)
        section_menu.focus_set()
        section_menu.pack(pady=5, padx=15, anchor="w", fill=tk.X)
        dialog.bind('<Down>', next_section)
        dialog.bind('<Up>',   next_section)
        # buttons
        bframe=tk.Frame(dialog)
        ok=tk.Button(bframe, text='OK', command=filter_section)
        dialog.bind('<Return>', filter_section)
        cancel=tk.Button(bframe, text='Cancel', command=dialog.destroy)
        dialog.bind('<Escape>', lambda e: dialog.destroy())

        bframe.pack(fill=tk.BOTH, padx=30, pady=10)
        ok.pack(side=tk.LEFT)
        cancel.pack(side=tk.RIGHT)
        dialog.transient()
        dialog.grab_set()
        dialog.wait_window()
        self.open_all()

    def open_all(self):
        for i in self.tree.get_children():
            self.tree.item(i, open=True)

    def close_all(self):
        for i in self.tree.get_children():
            self.tree.item(i, open=False)

    def create_widgets(self):
        self.tree=ttk.Treeview(self, selectmode='none')
        self.tree['columns']=('q', 'a', 'w')
        self.tree.column("#0", width=15, stretch=False)
        self.tree.column("q", width=150, minwidth=100)
        self.tree.column("a", width=150, minwidth=100)
        self.tree.column('w', width=55, stretch=False)
        self.tree.pack(side='left', fill=tk.BOTH, expand=True)
        scrollbar=tk.Scrollbar(self, command=self.tree.yview)
        self.tree.configure(yscrollcommand=scrollbar.set)
        scrollbar.pack(side='right', fill=tk.Y)
        self.tree.tag_configure('section',
                foreground='blue', background='#8af', font=(None, 12))
        self.tree.tag_configure('section0',
                foreground='#55f', background='#9bf', font=(None, 12))
        self.tree.tag_configure('comment',
                foreground='gray', background='#eeb', font=(None, 10))
        self.tree.tag_configure('black', foreground='black', font=(None, 12))
        self.tree.tag_configure('deleted',
                foreground='#faa', font=(None, 10, 'italic'))
        self.tree.tag_configure('moved',
                foreground='#990', font=(None, 13, 'bold'))
        self.tree.tag_configure('even', background='white', font=(None, 12))
        self.tree.tag_configure('odd',  background='#e0e0e0', font=(None, 12))

        self.tree.bind('<1>', self.tree_click)

    def tree_click(self, event):
        tree=event.widget
        column=tree.identify_column(event.x)
        iid=tree.identify_row(event.y)
        if not iid: return
        l=Line.iid_to_line[iid]
        if iid.startswith('w'):
            self.edit_word(l)
        elif iid.startswith('c') or column!='#0':
            self.edit_section(l)

    def create_lines(self):
        lines=[]
        s_index=0
        for s in db.all_sections:
            l=Line("section", s_index)
            lines.append(l)
            comment=''.join(db.comments[s]).rstrip()
            if comment:
                l=Line("comment", s_index, comment=comment)
                lines.append(l)
            for i in db.contents[s]:
                l=Line("word", s_index, i=i)
                lines.append(l)
            s_index += 1
        self.lines=lines
        self.filter=range(len(lines))
        self.display()

    def display(self):
        self.tree.delete(*self.tree.get_children())
        self.comments={}
        self.sections={}
        lines=[self.lines[i] for i in self.filter]
        
        for n, l in enumerate(lines):
            bg_tag='even' if n % 2 else 'odd'
            if l.ltype == "section":
                section_name=db.all_sections[l.s_index]
                tag='section'
                open_status=False
                if l.s_index == 0:
                    section_name='<no section>'
                    tag='section0'
                    open_status=True
                section=self.tree.insert("", tk.END,
                        values=(section_name, '', ''),
                        tags=(tag,), open=open_status, iid=l.iid)
                current_section=section
                self.sections[l.s_index]=section
            elif l.ltype == "comment":
                comment=self.tree.insert(current_section, tk.END,
                        values=(l.comment, '', ''), iid=l.iid,
                        tags=('comment',))
                self.comments[l.s_index]=comment
            else: # word
                t=db.times[l.i]
                f=db.fails[l.i]
                w=init.weight(t,f,1)
                w = w_string(w)
                word=self.tree.insert(current_section, tk.END,
                        values=(db.w1[l.i], db.w2[l.i], w),
                        tags=(bg_tag, l.color()),
                        iid=l.iid)

    def edit_word(self, l):
        dialog=EditWord(self, l)
        dialog.transient(self)
        dialog.wait_visibility()
        dialog.focus_force()
        dialog.grab_set()
        self.wait_window(dialog)

    def edit_section(self, l):
        dialog=EditSection(self, l)
        dialog.transient(self)
        dialog.wait_visibility()
        dialog.focus_force()
        dialog.grab_set()
        self.wait_window(dialog)

    def mark_as_not_saved(self):
        self.saved = False
        self.title('*' + self.filename)

class Line():
    iid_to_line={}
    def __init__(self, ltype, s_index, i=None, comment=None):
        self.ltype=ltype
        if ltype == "comment":
            self.comment=comment
            self.s_index=s_index
            self.iid="c" + str(s_index)
        if ltype == "section":
            self.s_index=s_index
            self.iid="s" + str(s_index)
        if ltype == "word":
            self.i=i
            self.marked=False
            self.s_index=s_index
            self.s_original=s_index
            self.iid="w" + str(i)
        self.iid_to_line[self.iid]=self
    
    def color(self):
        if self.marked:
            return 'deleted'
        elif self.s_index != self.s_original:
            return 'moved'
        else:
            return 'black'

class EditWord(tk.Toplevel):
    def __init__(self, master, l):
        super().__init__()
        self.master=master
        self.l=l
        i=l.i
        self.resizable(False, False)
        self.title('Word')
        # section
        movelabel=tk.Label(self, text='Move to:', anchor="w")
        movelabel.pack(pady="10 0", padx=15, fill=tk.X)
        self.section_variable=tk.StringVar(self)
        self.section_strings= ['<no section>'] + db.all_sections[1:]
        self.section_variable.set(self.section_strings[l.s_index])
        section_menu=tk.OptionMenu(self,
                self.section_variable, *self.section_strings)
        section_menu.pack(fill=tk.X, padx=10, pady="0 10")
        self.bind('<Down>', self.next_section)
        self.bind('<Up>',   self.previous_section)
        # question and answer entries
        wframe=tk.Frame(self)
        self.question=tk.StringVar(value=db.w1[i])
        ql=tk.Label(wframe, text=self.question.get())
        qe=tk.Entry(wframe, textvariable=self.question)
        self.answer=tk.StringVar(value=db.w2[i])
        al=tk.Label(wframe, text=self.answer.get())
        ae=tk.Entry(wframe, textvariable=self.answer)

        wframe.pack(pady=5, padx=10)
        ql.grid(row=0, column=0, padx=5)
        qe.grid(row=0, column=1, ipady=2)
        al.grid(row=1, column=0, padx=5)
        ae.grid(row=1, column=1, ipady=2)
        # delete
        self.delete_var=tk.BooleanVar()
        self.delete_var.set(l.marked)
        delete = tk.Checkbutton(self, variable=self.delete_var,
                text=" Mark this word to delete")
        delete.pack(anchor = "w", padx=8, pady=5)
        # find duplicates
        self.duplicates_var=tk.BooleanVar()
        self.duplicates_var.set(False)
        duplicates = tk.Checkbutton(self, variable=self.duplicates_var,
                text=" Find duplicates")
        duplicates.pack(anchor = "w", padx=8, pady="0 5")
        # statistics
        sframe=tk.Frame(self)
        self.times=tk.StringVar(value=db.times[i])
        tl=tk.Label(sframe, text="Times:")
        te=tk.Entry(sframe, textvariable=self.times, width=3)
        self.fails=tk.StringVar(value=db.fails[i])
        fl=tk.Label(sframe, text="Fails:")
        fe=tk.Entry(sframe, textvariable=self.fails, width=3)

        sframe.pack(pady=5, padx=8)
        tl.grid(row=0, column=0, padx=5)
        te.grid(row=0, column=1, ipady=2)
        fl.grid(row=0, column=2, padx=5)
        fe.grid(row=0, column=3, ipady=2)
        # buttons
        bframe=tk.Frame(self)
        ok=tk.Button(bframe, text='OK', command=self.apply_changes)
        self.bind('<Return>', self.apply_changes)
        cancel=tk.Button(bframe, text='Cancel', command=self.destroy)
        self.bind('<Escape>', lambda e: self.destroy())

        bframe.pack(fill=tk.BOTH, padx=30, pady=10)
        ok.pack(side=tk.LEFT)
        cancel.pack(side=tk.RIGHT)

    def next_section(self, event=None):
        n=len(db.all_sections)
        s=self.section_variable.get()
        s_index=self.section_strings.index(s)
        if s_index < n-1:
            self.section_variable.set(self.section_strings[s_index+1])

    def previous_section(self, event=None):
        n=len(db.all_sections)
        s=self.section_variable.get()
        s_index=self.section_strings.index(s)
        if s_index > 0:
            self.section_variable.set(self.section_strings[s_index-1])

    def find(self, event=None):
        finded=set()
        section=0
        question=self.question.get()
        answer=self.answer.get()
        for n, l in enumerate(self.master.lines):
            if l.ltype=="section":
                section=n
            elif l.ltype=="word":
                q=db.questions[l.i]
                a=db.answers[l.i]
                if question == q or answer == a:
                    finded.add(section)
                    finded.add(n)
        self.master.filter=sorted(finded)
        self.master.display()
        self.master.open_all()

    def apply_changes(self, event=None):
        self.master.refreshed=False
        self.master.mark_as_not_saved()
        i=self.l.i
        q=self.question.get()
        a=self.answer.get()
        t=self.times.get()
        f=self.fails.get()
        d=self.delete_var.get()
        s=self.section_variable.get()
        if not validate(q): q=db.w1[i]
        if not validate(a): a=db.w2[i]
        if not t.isdigit(): t=db.times[i]
        if not f.isdigit(): f=db.fails[i]
        t=int(t)
        f=int(f)
        if t==0: f==0
        w=init.weight(t, f, 1)

        self.master.tree.item(self.l.iid, values=(q, a, w_string(w)))

        self.l.marked=d
        self.l.s_index=self.section_strings.index(s)
        color=self.l.color()
        tags=self.master.tree.item(self.l.iid)['tags']
        tags[-1]=color
        self.master.tree.item(self.l.iid, tags=tags)

        db.w1[i]=q
        db.w2[i]=a
        db.times[i]=t
        db.fails[i]=f

        if self.duplicates_var.get():
            self.find()
        self.destroy()

class EditSection(tk.Toplevel):
    def __init__(self, master, l):
        super().__init__()
        self.resizable(False, False)
        self.title('Section')
        self.master=master
        self.l=l
        s_index=l.s_index
        section=db.all_sections[s_index]

        frame=tk.Frame(self)
        frame.pack(padx=20, pady=5)
        # comment
        commentl=tk.Label(frame, text="Add a comment to this section:")
        commentl.grid(row=0, column=0, sticky="w", pady='10 3')
        self.comment_widget=tk.Text(frame)
        self.comment_widget.grid(row=1, column=0, columnspan=2)
        self.comment_widget.config(height=3, width=50)
        comment=''.join(db.comments[section])
        self.comment_widget.insert(tk.END, comment)
        # section
        sectionl=tk.Label(frame, text="Change the name of the section:")
        sectionl.grid(row=2, column=0, sticky="w", pady='15 3')
        self.section_widget=tk.Entry(frame, font=(None, 14))
        if s_index==0:
            self.section_widget.insert(tk.END, '<no section>')
            self.section_widget.config(state='disabled')
        else:
            self.section_widget.insert(tk.END, section)
            self.section_widget.bind('<Return>',lambda e: self.ok.focus_set())
        self.section_widget.grid(row=3, column=0)
        # last word
        last_frame=ttk.Frame(frame)
        last_frame.grid(row=5, column=0, columnspan=2, sticky="ew", pady='15 3')
        add_label=tk.Label(last_frame, text="Add new words:  ")
        add_label.pack(side=tk.LEFT)
        self.last_word=tk.Label(last_frame, text="", fg='green')
        self.last_word.pack(side=tk.LEFT)
        self.delete_button=tk.Button(last_frame, font=(None, 9),
                text="delete last", command=self.delete_word)
        self.delete_button.pack(side=tk.RIGHT)
        self.delete_button.config(state='disabled')
        # add words
        self.new_questions=[]
        self.new_answers=[]
        self.question=ttk.Entry(frame, font=(None, 12))
        self.question.grid(row=6, column=0, sticky="ew", padx="0 10")
        self.question.focus_set()
        self.answer=ttk.Entry(frame, font=(None, 12))
        self.answer.grid(row=6, column=1, sticky="ew", padx="10 0")
        self.question.bind('<Return>', self.add_word)
        self.answer.bind('<Return>', self.add_word)
        # buttons
        self.ok=tk.Button(frame, text='OK', command=self.apply_changes)
        cancel=tk.Button(frame, text='Cancel', command=self.destroy)
        self.bind('<Escape>', lambda e: self.destroy())
        self.ok.bind('<Return>', lambda e: self.ok.invoke())
        self.ok.grid(row=7, column=0, pady="15 5")
        cancel.grid(row=7, column=1, pady="15 5")

    def add_word(self, event=None):
        question=self.question.get().strip()
        answer=self.answer.get().strip()
        if validate(question) and validate(answer):
            new_word=question + ' = ' + answer
            self.delete_button.config(state='normal')
            self.last_word.config(text=new_word)
            self.question.delete(0, tk.END)
            self.answer.delete(0, tk.END)
            self.question.focus_set()
            self.new_questions.append(question)
            self.new_answers.append(answer)
            
    def delete_word(self, event=None):
        if self.new_questions:
            self.new_questions.pop()
            self.new_answers.pop()
        if self.new_questions:
            new_word=self.new_questions[-1] + ' = ' + self.new_answers[-1]
            self.last_word.config(text=new_word)
        else:
            self.delete_button.config(state='disabled')
            self.last_word.config(text='')

    def apply_changes(self, event=None):
        self.master.refresh()
        self.master.mark_as_not_saved()
        s_index=self.l.s_index
        section=db.all_sections[s_index]
        # comments
        new_comment=[ line + '\n' 
                for line in self.comment_widget.get("1.0", tk.END).split('\n')
                if is_a_comment(line)
                ]
        new_comment=init.remove_duplicates(new_comment)
        db.comments[section]=new_comment

        # section
        new_section=self.section_widget.get().strip()
        if new_section == '' : new_section=section
        if new_section == init.default_section: new_section=section
        if new_section != section and s_index != 0:
            if new_section in db.all_sections:
                message='Section «{}» already exists'
                message=message.format(new_section)
                result=messagebox.showwarning(message=message)
                self.section_widget.delete(0, tk.END)
                self.section_widget.insert(tk.END, section)
                return
            db.all_sections[s_index]=new_section
            db.contents[new_section]=db.contents[section]
            db.comments[new_section]=db.comments[section]

        # words
        self.add_word()
        n=len(db.questions)
        for q, a in zip (self.new_questions, self.new_answers):
            db.contents[section].append(n)
            db.questions.append(q)
            db.answers.append(a)
            db.times.append(0)
            db.fails.append(0)
            n+=1

        self.destroy()
        self.master.create_lines()

class SearchDialog(tk.Toplevel):
    def __init__(self, father):
        super().__init__()
        self.father=father
        self.title('Search')
        # search
        sframe=tk.Frame(self)
        sframe.pack(fill=tk.X, pady=10, padx=12)
        label=tk.Label(sframe, text="Search:", anchor="w")
        label.pack(fill=tk.X, side='left')
        self.fuzzy_var=tk.BooleanVar()
        self.fuzzy_var.set(False)
        fuzzy=tk.Checkbutton(sframe,
                text=' Extended', var=self.fuzzy_var, anchor='w')
        fuzzy.pack(fill=tk.X, side='right')
        self.search_term=tk.StringVar()
        search=tk.Entry(self,
                textvariable=self.search_term, font=(None, 12), width=25)
        search.pack(padx=10)
        search.focus_set()
        # radio buttons
        rframe=tk.Frame(self)
        rframe.pack(padx=10, pady=10)
        self.where=tk.StringVar()
        self.where.set('both')
        r1=tk.Radiobutton(rframe,
                text='Questions', var=self.where, value='questions')
        r2=tk.Radiobutton(rframe,
                text='Answers', var=self.where, value='answers')
        r3=tk.Radiobutton(rframe,
                text='Both', var=self.where, value='both')
        r1.pack(side='left')
        r2.pack(side='left')
        r3.pack(side='left')
    
        # statistics
        sframe=tk.Label(self)
        sframe.pack(padx=10, pady=10)
        timesl=tk.Label(sframe, text="Times: from ")
        self.timesfrom=tk.Entry(sframe, width=3)
        self.timesfrom.insert(tk.END, "0")
        timesl2=tk.Label(sframe, text=" to ")
        self.timesto=tk.Entry(sframe, width=3)
        failsl=tk.Label(sframe, text="Fails:   from ")
        self.failsfrom=tk.Entry(sframe, width=3)
        self.failsfrom.insert(tk.END, "0")
        failsl2=tk.Label(sframe, text=" to ")
        self.failsto=tk.Entry(sframe, width=3)

        timesl.grid(row=0, column=0, sticky="w")
        self.timesfrom.grid(row=0, column=1)
        timesl2.grid(row=0, column=2)
        self.timesto.grid(row=0, column=3)
        failsl.grid(row=1, column=0, sticky="w")
        self.failsfrom.grid(row=1, column=1)
        failsl2.grid(row=1, column=2)
        self.failsto.grid(row=1, column=3)

        # buttons
        bframe=tk.Frame(self)
        ok=tk.Button(bframe, text='OK',command=self.find_filter)
        self.bind('<Return>', self.find_filter)
        cancel=tk.Button(bframe, text='Cancel', command=self.destroy)
        self.bind('<Escape>', lambda e: self.destroy())
        bframe.pack(fill=tk.BOTH, padx=30, pady=10)
        ok.pack(side=tk.LEFT)
        cancel.pack(side=tk.RIGHT)

    def find_filter(self, event=None):
        finded=set()
        search_term=self.search_term.get()
        times_from=self.timesfrom.get()
        times_to=self.timesto.get()
        fails_from=self.failsfrom.get()
        fails_to=self.failsto.get()
        tfrom = int(times_from) if times_from.isdigit() else None
        tto   = int(times_to)   if times_to.isdigit()   else None
        ffrom = int(fails_from) if fails_from.isdigit() else None
        fto   = int(fails_to)   if fails_to.isdigit()   else None

        lines=self.father.lines
        searchquestions = True
        searchanswers   = True
        where=self.where.get()
        if where == 'questions': searchanswers = False
        if where == 'answers': searchquestions = False
        for n, l in enumerate(lines):
            if l.ltype == 'section':
                section=n
            elif l.ltype == 'word':
                q = db.w1[l.i]
                a = db.w2[l.i]
                t = db.times[l.i]
                f = db.fails[l.i]
                if self.fuzzy_var.get():
                    qmatch = fuzzy_match(q, search_term)
                    amatch = fuzzy_match(a, search_term)
                else:
                    qmatch = search_term in q
                    amatch = search_term in a
                if (searchquestions and qmatch) or (searchanswers and amatch):
                    if in_range(t, tfrom, tto) and in_range(f, ffrom, fto):
                        finded.add(section)
                        finded.add(n)
        self.father.filter=sorted(finded)
        self.destroy()
        self.father.display()
        self.father.refreshed = False
        self.father.open_all()


def w_string(w):
    return '{:0.2f}'.format(w) if w > -1 else ''

def validate(string):
    return string!="" and not '=' in string

def fuzzy_match(string, term):
    index=0
    for i in term:
        index = string.find(i, index)
        if index == -1 : return False
        index +=1
    return True

def is_a_comment(line):
    if re.match(init.section_regexp, line):
        return False
    if re.match(init.word_regexp, line):
        return False
    if not re.search('[^\s]', line):
        return False
    return True

def average_up(a, b):
    "average or two numbers, rounded up"
    return (a+b+1)//2

def relative_path(absolute):
    p=Path(absolute)
    cwd=Path.cwd()
    try:
        relative=str(p.relative_to(cwd))
    except ValueError:
        relative=absolute
    return relative

def in_range(x, a, b):
    result=True
    if a != None:
        if x < a: result = False
    if b != None:
        if x > b: result = False
    return result
