import math

class Color():
    def __init__(self, r, g, b):
        self.r=float(r)
        self.g=float(g)
        self.b=float(b)

    def __add__(a, b):
        return Color(a.r + b.r, a.g + b.g, a.b + b.b)

    def __sub__(a, b):
        return a + (-1)*b

    def __rmul__(a, t):
        return Color(t*a.r, t*a.g, t*a.b)

    def __str__(a):
        b=255*a
        l=list(map(lambda i: int(round(i)), (b.r, b.g, b.b)))
        return "#{:02x}{:02x}{:02x}".format(*l)

    def contrasting(a):
        luminance=math.sqrt(0.299*a.r**2 + 0.587*a.g**2 + 0.114*a.b**2)
        return black if luminance > .55 else white


white=Color(1,1,1)
black=Color(0,0,0)
