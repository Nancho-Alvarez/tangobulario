#! /usr/bin/env python3

import sys
import tkinter as tk
import tkinter.ttk as ttk
from pathlib import Path
from tkinter.filedialog import askopenfilename

import color
import edit
import help
import init
import quiz

max_words_displayed=500

class TangobularioGui(tk.Tk):
    def __init__(self):
        global settings
        global db
        super().__init__()
        self.title("Tangobulario")
        self.resizable(False, True)
        self.protocol("WM_DELETE_WINDOW", self.terminate)

        self.create_widgets()
        settings=init.Settings()
        db=init.Database(settings.filename)
        if not db.ready:
            settings.sections=(0,)
        settings.db=db
        settings.sanitize()
        settings.names_to_sections()
        self.sections=settings.sections
        self.all_mode = settings.sections==(0,)
        
        self.update_options()
        self.old_sections=(0,)
        
        #self.edit()
        #sys.exit()

    def create_widgets(self):
        left_frame=tk.Frame(self)
        left_frame.pack(side='left', padx=20, pady=10, expand=True, fill=tk.Y)
        right_frame=tk.Frame(self)
        right_frame.pack(side='left', pady=10, expand=True, fill=tk.Y)

        # file
        file_frame=tk.Frame(left_frame)
        file_frame.pack(pady=5)
        open_dialog=tk.Button(file_frame,
                text='Open file', anchor="w", font=(None, 10), command=self.open_file)
        open_dialog.pack(side='left')
        self.file_entry=ttk.Entry(file_frame, font=(None, 12))
        self.file_entry.pack(fill=tk.X, side='left', padx=5)

        # number of questions
        nframe=tk.Frame(left_frame)
        nframe.pack(fill=tk.X, pady=10)
        nlabel=tk.Label(nframe, text="Number of words:", font=(None,12))
        nlabel.pack(side='left')
        self.nentry=ttk.Entry(nframe, width=4, justify='center', font=(None,12))
        self.nentry.bind('<Return>', self.update_n)
        self.nentry.bind('<FocusOut>', self.update_n)
        self.nentry.bind('<Escape>', self.update_n)
        self.nentry.pack(side='right', padx=5)

        # options
        self.reverse=tk.BooleanVar()
        reverse=tk.Checkbutton(left_frame,
                variable=self.reverse, command=self.update_reverse,
                text='Reverse question-answers', anchor='w', font=(None,12))
        reverse.pack(fill=tk.BOTH, pady=10)
        
        eframe=tk.Frame(left_frame)
        eframe.pack(fill=tk.X, pady=10)
        elabel=tk.Label(eframe, text="Exponent:   ", font=(None, 10))
        elabel.pack(fill=tk.BOTH, side='left')
        self.scale=tk.Scale(eframe, from_=-2, to=5, orient='horizontal', resolution=.1)
        self.scale.config(command=self.update_exp)
        self.scale.pack(fill=tk.BOTH, side='left', expand=True)

        # sections
        self.sections_box=tk.Listbox(
                left_frame, selectmode=tk.MULTIPLE, font=(None,14))
        self.sections_box.pack(pady=10, fill=tk.Y, expand=True)
        self.sections_box.bind('<<ListboxSelect>>', self.onboxselect)
       
        self.review=tk.BooleanVar()
        review=tk.Checkbutton(left_frame,
                variable=self.review, command=self.update_review,
                text='Last session review', anchor='w', font=(None,10))
        review.pack(fill=tk.BOTH, pady=0, padx=20)

        # sort
        sframe=tk.Frame(right_frame)
        sframe.pack(fill=tk.X, padx=6)
        self.sort_options=['Default order', 'Alphabetical', 'Weight']
        self.sort_variable=tk.StringVar(self)
        self.sort_variable.set(self.sort_options[0])
        sort_menu=tk.OptionMenu(sframe,
                self.sort_variable, *self.sort_options, command=self.change_order) 
        sort_menu.config(width=23, anchor='w')
        sort_menu.pack(side='left', fill=tk.X)
        self.reversed_sort=False
        self.sort_button=tk.Button(sframe, text="V", command=self.toggle_sort)
        self.sort_button.pack(side='left')

        # words
        frame=tk.Frame(right_frame)
        frame.pack(pady=5, padx=10, fill=tk.BOTH, expand=True)
        self.canvas=tk.Canvas(frame, width=250, relief='sunken', bd=2)
        scrollbar=tk.Scrollbar(frame, orient='vertical', command=self.canvas.yview)
        self.canvas.configure(yscrollcommand=scrollbar.set)
        self.canvas.pack(side='left', fill=tk.Y)
        scrollbar.pack(side='left', fill=tk.Y)
        self.words_frame=tk.Frame(self.canvas)
        self.counter=tk.Label(right_frame, anchor="w")
        self.counter.pack(side='top', fill=tk.X, padx=10)
        self.canvas.create_window((0,0), window=self.words_frame, anchor="nw")

        # paginator
        self.paginator=tk.Frame(right_frame)
        self.paginator.pack(pady='10 5')

        # buttons
        self.start_button = tk.Button(left_frame, 
                text='Start', command=self.start_quiz, font=(None, 14))
        self.start_button.pack(side="bottom", pady=25)
        self.start_button.focus_set()
        self.start_button.bind('<Return>', self.start_quiz)

        button_frame = tk.Frame(right_frame)
        button_frame.pack(pady=20, side='bottom')

        self.edit_button = tk.Button(
                button_frame, text='Edit', command=self.edit)
        self.edit_button.pack(side="left", padx=20)
        self.bind('e', self.edit)

        self.help_button = tk.Button(
                button_frame, text='Help', command=self.help)
        self.help_button.pack(side="left", padx=20)
        self.bind('h', self.help)
        self.bind('<F1>', self.help)

        self.quit_button = tk.Button(
                button_frame, text='Quit', command=self.terminate)
        self.quit_button.pack(side="left", padx=20)
        self.bind('q', self.terminate)
        self.bind('<Escape>', self.terminate)


    def update_options(self):
        self.file_entry.config(state='normal')
        self.file_entry.delete(0, tk.END)
        self.file_entry.insert(0, Path(settings.filename).name)
        self.file_entry.config(state='disabled')
        self.nentry.delete(0, tk.END)
        self.nentry.insert(0, settings.n)
        self.review.set(settings.review)
        self.reverse.set(settings.reverse)
        settings.reverse_questions()
        self.scale.set(settings.exponent)
        self.populate_sections()
        self.update_review()


    def populate_sections(self):
        self.sections_box.delete(0, tk.END)
        for s in db.all_sections:
            if s==init.default_section:
                s='<everything>'
            self.sections_box.insert(tk.END, s)
        for i in settings.sections:        
            self.sections_box.selection_set(i)
        self.word_indices=settings.questionables
        self.page=0
        self.create_list_of_words()
            
    def create_list_of_words(self):
        page=self.page
        mwd=max_words_displayed
        word_indices=self.word_indices
        range_w=''
        if len(word_indices) > mwd:
            min_w=page*mwd + 1
            max_w=min((page+1)*mwd, len(word_indices))
            range_w='  (' + str(min_w) + '-' + str(max_w) + ')'
        self.counter['text']=str(len(word_indices)) + range_w

        for w in self.words_frame.winfo_children():
            w.destroy()
        self.words=[]
        for i in word_indices[page*mwd :(page+1)*mwd]:
            l=tk.Label(self.words_frame, text=db.questions[i],
                    font=(None,11), anchor = "w", width=25)
            l.i=i
            l.bind('<Enter>',
                lambda e: e.widget.config(text=db.answers[e.widget.i]))
            l.bind('<Leave>',
                lambda e: e.widget.config(text=db.questions[e.widget.i]))
            l.pack(ipadx=20)
            self.words.append(l)
        if word_indices==[]:
            empty=tk.Label(self.words_frame,
                    text='empty', fg='#6060ff', font=(None, 10, 'italic'))
            empty.pack(fill=tk.X)
        self.calculate_colors()
        self.update()
        self.canvas.config(scrollregion=self.canvas.bbox('all'))
        self.enable_disable_start()

        # paginator
        for l in self.paginator.winfo_children():
            l.destroy()
        if len(word_indices) > mwd:
            tk.Label(self.paginator, text="Page: ").pack(side=tk.LEFT)
            for i in range(len(word_indices)//mwd + 1):
                lpage=tk.Label(self.paginator, text=str(i+1))
                if i != page:
                    lpage.config(fg='#6060ff')
                    lpage.bind('<1>', self.goto_page)
                else:
                    lpage['bg']='gray95'
                lpage.pack(side=tk.LEFT, padx=7)

    def goto_page(self, event=None):
        self.page=int(event.widget['text'])-1
        self.create_list_of_words()

    def calculate_colors(self):
        mwd=max_words_displayed
        page=self.page
        weights=[settings.weights(i) for i in self.word_indices]
        sw=set(weights + [-1])
        sw=sorted(list(sw))
        try:
            mx=sw[-1]
            mn=sw[1]
            d=mx-mn
        except IndexError:
            d=0
        white =color.white
        color1=color.Color(0,.8,0) # green
        color2=color.Color(.8,0,0) # red
        color3=color.Color(1,1,0.25) # yellow (middle color)
        colord1=color3 - color1
        colord2=color2 - color3
        for i, w in enumerate (weights[page*mwd:(page+1)*mwd]):
            if w<0:
                c=white
            else:
                if d==0:
                    t = 0.5
                else:
                    t = (1/d)*(w-mn)
                    t= t**0.3 # to smooth the gradient
                # two gradients:
                # from color1 to color3, and from color3 to color2
                if t <= 0.5:
                    c=color1 + 2*t*colord1
                else:
                    c=color3 + (2*t-1)*colord2
            bgc=str(c)
            fgc=str(c.contrasting())
            self.words[i].config(bg=bgc, fg=fgc)
        return

    def enable_disable_start(self):
        if self.word_indices:
            self.start_button['state']="normal"
        else:
            self.start_button['state']="disabled"

    def change_order(self, option):
        l=self.word_indices
        functions={
            self.sort_options[0]: None,
            self.sort_options[1]: lambda i:  db.questions[i].lower(),
            self.sort_options[2]: lambda i: -settings.weights(i) }
        rev=self.reversed_sort
        l=sorted(l, key=functions[option], reverse=rev)
        self.word_indices=list(l)
        self.create_list_of_words()

    def toggle_sort(self):
        arrows=['Ʌ', 'V']
        arrow=arrows[int(self.reversed_sort)]
        self.reversed_sort = not self.reversed_sort
        self.sort_button['text']=arrow
        self.change_order(self.sort_variable.get())

    def update_n(self, event=None):
        a=self.nentry.get()
        try:
            n=int(a)
        except ValueError:
            n=settings.n
        if n>80: n=80
        if n< 1: n= 1
        settings.n=n
        self.nentry.delete(0, tk.END)
        self.nentry.insert(0, settings.n)
        self.start_button.focus_set()

    def update_exp(self, e):
        settings.exponent=float(e)
        self.calculate_colors()

    def update_review(self):
        v=self.review.get()
        settings.review=v
        if v:
            self.sections_box.config(state='disabled')
            self.word_indices=settings.review_list
        else:
            self.sections_box.config(state='normal')
            self.word_indices=settings.questionables
        self.create_list_of_words()

    def update_reverse(self):
        v=self.reverse.get()
        settings.reverse=v
        settings.reverse_questions()
        self.create_list_of_words()

    def onboxselect(self, event):
        box=self.sections_box
        s=box.curselection()
        if s == ():
            # there is a very strange bug when there are
            # two or more instances of the program running.
            # It seems as if the event '<<ListboxSelect>>' affects
            # both instances, and somehow self.old_sections is not
            # defined
            try:
                for i in self.old_sections:
                    box.selection_set(i)
            except AttributeError:
                print('Only one instance of the program allowed')
                sys.exit()
            s=self.old_sections
        elif self.all_mode and len(s)==2:
            self.old_sections=(0,)
            box.selection_clear(0)
            s=s[1:]
            self.all_mode=False
        elif 0 in s:
            self.old_sections=s[1:]
            for i in self.old_sections:
                box.selection_clear(i)
            s=(0,)

        self.all_mode = s==(0,)
        settings.sections=s
        self.word_indices=settings.questionables
        self.page=0
        self.change_order(self.sort_variable.get())
        # change_order calls create_list_of words()


    def open_file(self, name=None, saveini=True):
        global db
        if not name:
            absolute=askopenfilename(
                    title="Select words database:",
                    filetypes=(("txt files", "*.txt"), ("all files", "*.*")))
        else:
            absolute=name
        if absolute:
            if saveini:
                settings.save_ini()
            relative=edit.relative_path(absolute)
            db=init.Database(absolute)
            settings.db=db
            settings.filename=relative
            settings.interpret_ini()
            settings.review=False
            self.update_options()

    def help(self, event=None):
        self.help_button.config(state='disabled')
        h=help.HelpWindow(self)

    def edit(self, event=None):
        self.withdraw()
        settings.save_ini()
        e=edit.Edit(settings)
        e.grab_set()
        self.wait_window(e)
        self.deiconify()
        self.open_file(settings.filename, saveini=False)

    def start_quiz(self, event=None):
        self.update_n()
        exam=quiz.Exam(settings, self)
        exam.grab_set()
        self.withdraw()
        self.quit_signal=False
        self.wait_window(exam)
        if self.quit_signal:
            self.terminate()
        else:
            self.deiconify()
            self.calculate_colors()

    def terminate(self, event=None):
        settings.save_ini()
        self.destroy()

root=TangobularioGui()
root.mainloop()
