import configparser
import re
import sys
from itertools   import accumulate
from collections import defaultdict
from random      import random

default_section = "_default"
default_n       = 20
default_e       = 1.5
default_ini     = "config.ini"
section_regexp  = r'^\s*\[\s*([^=\s][^=]+)\s*\]\s*$'
word_regexp     = r'^\s*([^=]+)=([^=]+)(=(\d+)=(\d+))?\s*$' 

class Settings:
    def __init__(self):
        global config
        global parsed
        config=read_ini()
        parsed=parse()
        if 'filename' in parsed:
            self.filename=parsed['filename']
        else:
            self.filename=config['DEFAULT']['last']
        self.interpret_ini()
        self.override_with_parsed()

    def override_with_parsed(self):
        self.review=parsed['review']
        if 'n' in parsed:
            self.n=parsed['n']
        if 'exponent' in parsed:
            self.exponent=parsed['exponent']
        if 'reverse' in parsed:
            self.reverse=parsed['reverse']
        self.section_names=parsed.get('section_names', set())

    def sanitize(self):
        """In case that the ini config is not compatible with the actual file,
        for example if some words or sections were deleted from the file,
        we fallback to default values"""
        max_section=max(self.sections)
        max_word=max(self.review_list, default=-1)
        if max_word > len(self.db.w1) - 1:
            self.review_list=[]
        if max_section > len(self.db.all_sections) - 1:
            self.sections=(0,)

    def names_to_sections(self):
        if self.section_names:
            db=self.db
            n=len(db.all_sections)
            l=[i for i in range(n) if db.all_sections[i] in self.section_names]
            s=tuple(l)
            if not s: s=(0,)
            self.sections = s

    @property
    def questionables(self):
        sections=list(self.sections)
        if sections == [0]:
            sections=list(range(len(self.db.all_sections)))
        l=[]
        for i in sections:
            s=self.db.all_sections[i]
            l += self.db.contents[s]
        return l

    def pick(self):
        questionables=self.questionables
        sample_size=min(self.n, len(questionables))
        if sample_size==0:
            return []
        weights=[self.weights(i) for i in self.questionables]
        m=max(weights)
        if m==-1: m=1 # all the words are new
        weights=[m if i == -1 else i for i in weights]
        sample=choice(questionables, sample_size, weights)
        return sample

    def reverse_questions(self):
        if self.reverse:
            self.db.questions=self.db.w2
            self.db.answers=self.db.w1
        else:
            self.db.questions=self.db.w1
            self.db.answers=self.db.w2

    def weights(self, i):
        e=self.exponent
        return weight(self.db.times[i], self.db.fails[i], e)

    def interpret_ini(self):
        ini_section=self.filename
        if not config.has_section(ini_section):
            config[ini_section]={}
        self.n=int(config[ini_section]['n'])
        self.exponent=float(config[ini_section]['e'])
        self.reverse=config[ini_section]['r']=='True'
        s=config[ini_section]['s']
        self.sections=tuple(str2list(s))
        v=str2list(config[ini_section]['review_list'])
        self.review_list=v
        self.current_review_list=v

    def save_ini(self):
        ini_section=self.filename
        if not ini_section: return
        if not config.has_section(ini_section):
            config[ini_section]={}
        config['DEFAULT']['last']=ini_section
        config[ini_section]['n']=str(self.n)
        config[ini_section]['e']=str(self.exponent)
        config[ini_section]['r']='True' if self.reverse else 'False'
        # do not save the review flag in the ini
        #config[ini_section]['v']='True' if self.review else 'False'
        s=self.sections
        config[ini_section]['s']=seq2str(s)
        v=self.current_review_list
        config[ini_section]['review_list']=seq2str(v)
        with open(default_ini, 'w') as f:
            config.write(f)


class Database:
    def __init__(self, filename=None):
        self.w1=[]
        self.w2=[]
        self.times=[]
        self.fails=[]
        self.contents={default_section: []}
        self.all_sections=[default_section]
        self.contents=defaultdict(list)
        self.comments=defaultdict(list)
        if filename == None:
            self.lines=['\n']
        else:
            try:
                self.load(filename)
                self.arrange_indices()
                self.ready=True
            except FileNotFoundError:
                self.ready=False
        self.questions=self.w1
        self.answers  =self.w2

    def load(self, filename):
        with open(filename) as file:
            self.lines=file.readlines()
        self.lines.append('\n')
        number_of_lines=len(self.lines)
        self.w1    = [""]*number_of_lines
        self.w2    = [""]*number_of_lines
        self.times = [0] *number_of_lines
        self.fails = [0] *number_of_lines
        comments=[]
        section=default_section
        self.all_sections=[section]
        top_of_file=True
        first_blank_line=True
        for i, line in enumerate(self.lines):
            m = re.search(section_regexp, line)
            if m: # section
                section=m.group(1).strip()
                if section not in self.all_sections:
                    self.all_sections.append(section)
                self.comments[section].extend(comments)
                comments=[]
                top_of_file = False
                first_blank_line = False
            else:
                m = re.search(word_regexp, line)
                if m: # word
                    self.w1[i]=m.group(1).strip()
                    self.w2[i]=m.group(2).strip()
                    if m.group(3):
                        self.times[i]=int(m.group(4))
                        self.fails[i]=int(m.group(5))
                    self.contents[section].append(i)
                    if top_of_file:
                        top_of_file=False
                        self.comments[section].extend(comments)
                    comments=[]
                    first_blank_line = False
                else: # comment
                    if re.search('[^\s]', line):
                        comments.append(line)
                    else:
                        if first_blank_line:
                            # to separate global comments from
                            # section comments we use a blank line
                            self.comments[default_section].extend(comments)
                            comments=[]
                            first_blank_line = False
        # remove repeated comments
        for s, c in self.comments.items():
            self.comments[s]=remove_duplicates(c)


    def arrange_indices(self):
        """Instead of using as indices the line numbers of the file,
        we remap these indices to a range of the total number of words.
        This groups together words in repeated sections, avoids blank lines
        and simplifies the creation of review lists."""
        l=[]
        for s in self.all_sections:
            l.extend(self.contents[s])
        rev=dict((j, i) for i, j in enumerate(l))
        for s in self.all_sections:
            self.contents[s]=[rev[i] for i in self.contents[s]]
        self.w1   =[self.w1[i]    for i in l]
        self.w2   =[self.w2[i]    for i in l]
        self.times=[self.times[i] for i in l]
        self.fails=[self.fails[i] for i in l]


    def save(self, filename):
        with open(filename, 'w') as file:

            # this is to distinguish between global comments and 
            # section comments
            no_default_section = self.contents[default_section]==[]
            if (
                    self.contents[default_section]==[] and
                    len(self.all_sections)>1 and
                    self.comments[self.all_sections[1]] and
                    not self.comments[default_section]
                ):
                    file.write('\n')

            for s in self.all_sections:
                if self.comments[s]:
                    for c in self.comments[s]:
                        file.write(c)
                    file.write('\n')
                if s != default_section:
                    section='[' + s + ']\n'
                    file.write(section)
                for i in self.contents[s]:
                    text=self.w1[i] + '=' + self.w2[i]
                    if self.times[i]:
                        text += '='+str(self.times[i])+'='+str(self.fails[i])
                    text += '\n'
                    file.write(text)
                if self.contents[s] or s != default_section:
                    file.write('\n')

    def lines_after_comment(self):
        "Returns the list of lines of the file, excluding the first comment"
        n=len(self.lines)
        for l in self.lines:
            if (re.match(section_regexp,l) or
                    re.match(word_regexp,l) or l.strip() == ''):
                n=self.lines.index(l)
                break
        return self.lines[n:]


def weight(times, fails, e):
    """
    The return is the relative probability of each word:
    for example, a word with return value of 3 
    is 6 times more likely to be chosen
    than a word with return value 0.5
    """
    if times==0: return -1
    return ((1+fails)/times) ** e


def choice(pob, sample_size, weights):
    """Choose <sample_size> elements from a population <pob> without
    replacement. It is a equivalent to the function from numpy.random
    choice(pob, sample_size, p=weights, replacement=False)"""
    p=pob.copy()
    w=weights.copy()
    l=[]
    for n in range(sample_size):
        cum_w=list(accumulate(w))
        s=cum_w[-1]
        x=s*random()
        i=0
        for cw in cum_w:
            if cw>x: break
            i += 1
        l.append(p.pop(i))
        w.pop(i)
    return l

def read_ini():
    conf=configparser.ConfigParser()
    try:
        conf.read_file(open(default_ini))
    except FileNotFoundError:
        conf['DEFAULT']={
                'n': default_n,
                'e': default_e,
                'r': 'False',
                's': '0',
                'last': 'words.txt',
                'review_list': ''}
        conf['words.txt']={}
    return conf
  
def str2list(string):
    s=string.split()
    return list(map(int, s))

def seq2str(l):
    ll=map(str, l)
    return ' '.join(ll)



def parse():
    parsed={}
    section_scan=False
    exponent_scan=False
    parsed['review']=False
    for a in sys.argv[1:]:
        if re.match('^-\d+$', a) and not exponent_scan:
            parsed['n']=int(a[1:])
        elif a=='-r': parsed['reverse']=True
        elif a=='-v': parsed['review']=True
        elif a=='-s': section_scan=True
        elif a=='-e': exponent_scan=True
        else:
            if section_scan:
                parsed['section_names']=set(a.split(','))
                section_scan=False
            elif exponent_scan:
                try:
                    parsed['exponent']=float(a)
                    exponent_scan=False
                except ValueError:
                    parsed['exponent']=default_e
            else:
                parsed['filename']=a
    return parsed

def remove_duplicates(old):
    new=[]
    for i in old:
        if not i in new:
            new.append(i)
    return new


